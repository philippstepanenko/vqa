import os
import sys
import hashlib

# логирование
def log(text = ''):
    print(f'{text}' )

# функция для получения хэш-суммы файла
def getHashFile(pathName, hashType = ''):
    
    # загрузка файла
    data = loadFile(pathName)
    if data == False:
        return 'NOT FOUND'
    data = data.encode('utf-8')
    
    # получение хэш-суммы
    calc = getHash(data, hashType)
    if calc == False:
        return 'ERROR HASH TYPE'
    
    return calc

# функция сравнения хэша
def copmareHash(calcHash, oldHash):
    if calcHash in ['OK','FAIL','NOT FOUND']:
        log(f'{fileName} {calcHash}')
    elif calcHash == oldHash:
        log(f'{fileName} OK')
    else:
        log(f'{fileName} FAIL')

# функция возвращающая содержимое файла
def loadFile(fileName):
    
    # если файл существует
    if os.path.exists(fileName):
        
        # открываем файл для чтения
        with open(fileName, 'r') as file:
            data = file.read()
        return data
    
    return False

# функция для вычисления хэш-суммы
def getHash(data, hashType = ''):
    
    # доступные методы хеширования по условию задачи
    hashes = ['md5', 'sha1', 'sha256']
    
    # поиск метода хэширования в списке доступных
    if hashType in hashes:
        
        # вызов метода по названию из списка методов
        res = hashlib.new(hashType)
        res.update(data)
        res = res.hexdigest()
        
        # если требуется большая скорость, то
        # лучше вызывать методы хэширования явно
        # (например, hashlib.md5(data))
        
        return res
    else:
        return False

def main():
    # проверка наличия нужного количества параметров у скрипта
    if len(sys.argv) == 3 :
        pathFile = sys.argv[1]
        pathDir = sys.argv[2]
        
        #получение списка данных по файлам
        listFiles = loadFile(pathFile)

        if listFiles == False:
            log('incorrect path to the input file')
            return False
        
        listFiles = listFiles.split('\n')

        # перебор данных
        for file in listFiles:
            
            # получение данных из строки
            fileName, typeHash, oldHash = file.split(' ')
            pathName = os.path.join(pathDir, fileName)

            # получение хэша файла
            calcHash = getHashFile(pathName, typeHash)
            
            # если получены ошибки на этапе вычисления хэша
            if calcHash in ['NOT FOUND', 'ERROR HASH TYPE']:
                log(f'{fileName} {calcHash}')
            elif calcHash == oldHash:
                log(f'{fileName} OK')
            else:
                log(f'{fileName} FAIL')
            
    else:
        log('Args not found:')
        log('path to the input file')
        log('path to the directory containing the files to check')

if __name__ == '__main__':
    main()
