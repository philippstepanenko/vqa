import os
import sys
import xml.etree.ElementTree as ET
import shutil
import datetime

# логирование
def log(text = ''):
    today = datetime.datetime.now()
    print(f'{today.strftime("%d-%m-%Y %H:%M:%S")} {text}' )

# проверка файла на существование
def fileExists(path = '', name = ''):
    file = os.path.join(path, name)
    if os.path.exists(file):
        return True
    else:
        return False

# проверка конфигурационного файла на существование
def configExists(config = ''):
    path = os.path.abspath(__file__)
    return os.path.exists(config) or os.path.exists(os.path.join(path, config))

# копирование файла
def copyFile(filePath = '', fileName = '', destPath = ''):
    
    # проверка исходного файла на существование
    if fileExists(filePath, fileName):
        
        try:
            source = os.path.join(filePath, fileName)
            copy = os.path.join(destPath, fileName)
            shutil.copy2(source, copy)
            log(f'Файл "{fileName}" скопирован из "{filePath}" в "{destPath}"')
            return True
        
        except:
            log('Не удалось скопировать файл')
            return False
    else:
        log('Исходный файл не существует')
        return False
        

# получить данные в виде словаря
def getData(dataFile):
    data = {}
    attrsXML = ['source_path','file_name','destination_path']
    attrsOBJ = ['filePath','fileName','destPath']
    for i, j in zip(attrsXML,attrsOBJ):
        try:
            data[j] = dataFile.attrib[i]
        except:
            data[j] = False
    return data

def checkData(data):
    if False in data:
        log('Недостаточно данных для копирования')
        return False
    else:
        return True

# функция получения конфига из параметра или по умолчанию
def getConfig():
    
    # попытка получить конфиг в виде параметра
    if len(sys.argv) == 2:
        
        # получение конфига
        configName = sys.argv[1]
        log(f'Попытка загрузить конфиг {configName}')
        
    # если конфиг не получен        
    else:
        
        # конфиг по умолчанию
        defaultConfig = 'config.xml'
        configName = defaultConfig
        log(f'Загрузка конфига по умолчанию ({configName})')
        
    # проверка существования конфигурационного файла
    if configExists(configName):
        return configName
    else:
        log('Конфиг не найден')
        return False
        

def main():

    # получение конфига
    configName = getConfig()
        
    # проверка загрузки конфига
    if configName == False:
        return
    
    # парсинг конфигурационного файла
    tree = ET.parse(configName)
    root = tree.getroot()

    # перебор данных о файлах в цикле
    for file in root:

        # получение необходимых данные для копирования файла
        data = getData(file)

        # проверка на полноту данных
        if checkData(data):

            # деструктуризация объекта с данными
            filePath, fileName, destPath = list(data.values())
            
            # копирование файла
            copyFile(filePath, fileName, destPath)

if __name__ == '__main__':
    main()
