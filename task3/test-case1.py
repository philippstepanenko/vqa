import vtest

# prep
from datetime import datetime
from pathlib import Path

# run
from os import listdir
from os.path import isfile
from os.path import join as joinpath

'''
	[prep] Если текущее системное время,
        заданное как целое количество секунд от начала эпохи Unix,
        не кратно двум, то необходимо прервать выполнение тест-кейса.
        
	[run] Вывести список файлов из домашней директории текущего пользователя.

	[clean_up] Действий не требуется.
'''

class TestCase1(vtest.VTest):
    def __init__(self, tc_id, name):
        super().__init__(tc_id, name)

    def prep(self):
        unixtime = int(datetime.now().timestamp())
        return unixtime % 2 == 0
    
    def run(self):
        home = str(Path.home())
        for file in listdir(home):
            if isfile(joinpath(home,file)):
                print(file)
        return True

test = TestCase1(1, 'Список файлов')

if __name__ == '__main__':
    test = TestCase1(1, 'Список файлов')
    test.execute()
