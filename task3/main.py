import sys
import os
import types
import vtest
import time
import datetime

# класс обработчик тест-кейсов
class TestRunner:
    def __init__(self, args):
        self.names = args    # параметры при запуске тестовой системы
        self.tests = []      # список тест-кейсов
        self.errors = []     # список несуществующих тест-кейсов

    # сохранить информацию в логи
    def saveLog(self, text=''):
        with open('log.txt','a') as log:
            log.write(text)
    
    # печать log
    def log(self, text=''):
        self.saveLog(str(text)+'\n')
        print(text)

    # добавить тест-кейс в список на выполнение
    def addTestCase(self, testCase):
        testModule = self.importModule(testCase)
        if not testModule == False:
            self.tests.append(testModule)
        else:
            self.errors.append(testCase)

    # добавление тест-кейсов из списка параметров
    def addTestCases(self):
        for name in self.names:
            self.addTestCase(name)

    # функция импорта кода
    def importCode(self,code, name):
        # создаём пустой модуль
        module = types.ModuleType(name)
        # динамическое выполнение
        exec(code, module.__dict__)
        return module

    # импорт модуля по имени в виде строки
    def importModule(self,fileName):
        # проверка наличия модуля
        path = sys.path[0]
        name = f'{fileName}.py'
        fullPath = os.path.join(path,name)
        if os.path.exists(fullPath):
            # загрузка кода модуля
            with open(name, 'r') as file:
                        code = file.read()
            # импорт кода
            return self.importCode(code, fileName)
        else:
            return False

    # запуск 
    def run(self):
        # учёт статистики выполнения тест-кейсов
        statsOK = 0
        statsFail = 0
        
        # загрузка тест-кейсов
        self.addTestCases()
        
        # проверка перед стартом
        today = datetime.datetime.now()
        self.log(today.strftime('%d-%m-%Y %H:%M:%S'))
        self.log('Runner starting...')
        self.log(f'test-cases: {self.tests}')
        self.log(f'undefined: {self.errors}')
        self.log('=' * 84)
        if len(self.tests) > 0:
            # засекаем время
            start_time = time.time()
            # обход тест-кейсов
            for i in self.tests:
                
                # получение тест-кейса
                testCase = i.test
                
                # получение результата работы тест-кейса
                self.log(f'{testCase} starting...')
                self.log()
                res = testCase.execute()
                
                # если тест-кейс успешно завершился
                if res == True:
                    self.log(f'{i} ... OK')
                    statsOK += 1
                else:
                    self.log(f'{i} ... FAIL')
                    
                    statsFail += 1
                    
                    self.log('Log:')
                    self.log(testCase.log)
                    
                self.log('=' * 84)
                
            finale_time = (time.time() - start_time)
            self.log('Complete')
            self.log(f'Ran {len(self.tests)} tests in {round(finale_time,3)}s')
            self.log(f'OK: {statsOK}')
            self.log(f'FAIL: {statsFail}')
            self.log()
        else:
            self.log('Undefined test-cases')
        

if __name__ == '__main__':
    if len(sys.argv) > 1:
        args = sys.argv[1:]
        testRunner = TestRunner(args)
        testRunner.run()
    else:
        print('Args not found')
