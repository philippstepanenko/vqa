# класс для реализации классов тест-кейсов
class VTest:
    def __init__(self, tc_id, name):
        self.tc_id = tc_id
        self.name = name
        self.methods = ['prep', 'run', 'clean_up']
        self.log = []

    # добавить log
    def addLog(self, text):
        self.log.append(text)

    # отобразить logs
    def printLog(self):
        for log in self.log:
            print(log)
    
    # метод для подготовки тестов
    def prep(self):
        return True

    # метод для выполнения тестов
    def run(self):
        return True

    # метод для завершения тестов
    def clean_up(self):
        return True

    # вспомогательный метод для логов работы методов
    def helperReturned(self, method, res):
        self.addLog(f'{method} returned {res}')
        
    # вспомогательный метод для логов исключений
    def helperCrashed(self, method):
        self.addLog(f'{method} crashed')
        
    # всмпомогательный метод обработчик
    def handler(self):
        # перебор методов
        for method in self.methods:
            try:
                # вызов метода по названию из списка методов
                res = getattr(self, method)()
                self.helperReturned(method, res)
                if res == False:
                    return False
            except:
                self.helperCrashed(method)
                return False
        return True
    # метод выполнения тест-кейса
    def execute(self):
        if self.handler() == True:
            self.addLog('OK')
            self.printLog()
            return True
        else:
            self.addLog('Fail')
            self.printLog()
            return False
        
        
    

