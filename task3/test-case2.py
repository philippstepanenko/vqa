import vtest

# prep
import psutil

# run
import random

# clean_up
import os
import sys

'''
	[prep] Если объем оперативной памяти машины, на которой исполняется тест,
	меньше одного гигабайта, то необходимо прервать выполнение тест-кейса.
	
	[run] Создать файл test размером 1024 КБ со случайным содержимым.
	
	[clean_up] Удалить файл test.

'''

class TestCase(vtest.VTest):
    def __init__(self, tc_id, name):
        super().__init__(tc_id, name)

    def prep(self):
        total = psutil.virtual_memory().total / 1024 ** 3
        return total >= 1
    
    def run(self):
        with open('test.txt', 'w+') as file:
            n = 1024
            s = ''
            a = 32
            b = 126
            for i in range(n):
                c = random.randint(a,b)
                s += chr(c)
            file.write(s)
        return True
    
    def clean_up(self):
        name ='test.txt'
        path = sys.path[0]
        fpath = os.path.join(path, name)
        os.remove(fpath)
        return True

test = TestCase(2, 'Случайный файл')

if __name__ == '__main__':
    test = TestCase(2, 'Случайный файл')
    test.execute()
